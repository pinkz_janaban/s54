let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

class Queue {

	constructor(){
		this.elements = {};
		this.head = 0;
		this.tail = 0;
	}

	enqueue(element){
		this.elements[this.tail] = element;
		this.tail++;
	}

	dequeue(){
		const item = this.elements[this.head];
		delete this.elements[this.head];
		this.head++;
		return item;
	}

	front(){
		return this.elements[this.head];
	}

	print(){
		for(let i = 0; i < this.head; i++){
			console.log(this.element[i]);
		}
	}

	get length(){
		return this.tail - this.head;
	}

	isEmpty(){
		if(this.length == 0){
			return true;
		}
		else{
			return false
		}
	}

}

let queue = new Queue();

console.log(queue.print());
queue.enqueue('John');
// console.log(queue);
queue.enqueue('Jane');
// console.log(queue);
let firstOut = queue.dequeue();
// console.log('First out is ' + firstOut);
// console.log(queue)
queue.enqueue('Bob');
// console.log(queue);
queue.enqueue('Cherry');
//console.log(queue);
console.log(queue.front());
console.log(queue.length);
console.log(queue.isEmpty());
console.log(queue.print());


module.exports = {
	//export created queue functions
	print,
};